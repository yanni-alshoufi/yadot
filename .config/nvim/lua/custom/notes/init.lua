local telescope = require 'telescope.builtin'

local notes_path = '/Users/yanni/.config/nvim/notes/'
local note_extention = 'md'

require 'custom.input-helpers.yes-no'

function YaOpenNote()
    telescope.find_files {
        cwd = notes_path,
    }
end

function YaAddNote()
    local name = vim.fn.input 'Name (without extention): '
    local path = notes_path .. vim.trim(name) .. '.' .. note_extention

    os.execute("echo '# " .. vim.trim(name) .. "' > '" .. path .. "'")

    YaYesNoInp('open note', function()
        vim.cmd('e ' .. path)
    end)
end

function YaDeleteNote()
    YaDoWithFileInDir(notes_path, function(path)
        YaYesNoInp('delete note', function()
            os.execute("rm '" .. path .. "'")
        end)
    end)
end
