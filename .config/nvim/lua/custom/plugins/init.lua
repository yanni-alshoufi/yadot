return {
    {
        'tjdevries/colorbuddy.nvim',
    },
    {
        '2nthony/vitesse.nvim',
        dependencies = {
            'tjdevries/colorbuddy.nvim',
        },
        opts = {
            comment_italics = true,
            transparent_background = true,
            transparent_float_background = true, -- aka pum(popup menu) background
            reverse_visual = false,
            dim_nc = false,
            cmp_cmdline_disable_search_highlight_group = false, -- disable search highlight group for cmp item
            -- if `transparent_float_background` false, make telescope border color same as float background
            telescope_border_follow_float_background = false,
            -- similar to above, but for lspsaga
            lspsaga_border_follow_float_background = false,
            -- diagnostic virtual text background, like error lens
            diagnostic_virtual_text_background = false,

            -- override the `lua/vitesse/palette.lua`, go to file see fields
            colors = {},
            themes = {},
        },
    },

    -- F# support
    {
        'ionide/Ionide-vim',
        ft = { 'fs', 'fsx', 'fsproj' },
        lazy = false,
    },
    -- F# syntax highlighting
    {
        'adelarsq/neofsharp.vim',
        ft = { 'fs', 'fsx', 'fsproj' },
        lazy = false,
    },

    -- Surround
    {
        'kylechui/nvim-surround',
        version = '*', -- Use for stability; omit to use `main` branch for the latest features
        event = 'VeryLazy',
        opts = {},
    },

    -- Git Blame
    {
        'FabijanZulj/blame.nvim',
        config = function()
            require('blame').setup {
                blame_type = 'virtual',
            }
        end,
        lazy = false,
    },

    -- Lazygit
    {
        'kdheepak/lazygit.nvim',
        cmd = {
            'LazyGit',
            'LazyGitConfig',
            'LazyGitCurrentFile',
            'LazyGitFilter',
            'LazyGitFilterCurrentFile',
        },
        -- optional for floating window border decoration
        dependencies = {
            'nvim-lua/plenary.nvim',
        },
        -- setting the keybinding for LazyGit with 'keys' is recommended in
        -- order to load the plugin when the command is run for the first time
        keys = {
            { '<leader>gg', '<CMD>LazyGit<CR>', desc = 'LazyGit' },
        },
    },

    -- Inlay Hints
    {
        'simrat39/inlay-hints.nvim',
        opts = {},
    },

    -- Tree
    {
        'nvim-tree/nvim-tree.lua',
        opts = {},
    },
}
