function YaProjSearch()
    YaDoWithFileInDir('/Users/yanni/local-projects', function(path)
        vim.print(path)
        vim.cmd('e ' .. path)
    end)
end

function YaCreateProject()
    local project_name = vim.fn.input 'Name of project: '
    local project_language = vim.fn.input 'Language of project: '

    project_name = vim.trim(project_name)
    project_language = vim.trim(project_language)

    YaYesNoInp('create the new project', function()
        os.execute("mklp '" .. project_name .. "' '" .. project_language .. "'")
        YaProjSearch()
    end)
end

function YaDeleteProject()
    YaDoWithFileInDir('/Users/yanni/local-projects', function(path)
        YaYesNoInp('move project to project trash', function()
            os.execute("mv '" .. path .. "' /Users/yanni/local-projects-trash")
        end)
    end)
end
