local M = {
    -- GIT
    -- Blame
    { 'n', '<Leader>gb', '<CMD>BlameToggle<CR>', { silent = true } },
}

return M
