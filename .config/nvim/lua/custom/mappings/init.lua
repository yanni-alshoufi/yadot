---@type string
local prefix = ... .. '.' -- prefix to current module

local M = {
    require(prefix .. 'general'),
    require(prefix .. 'code'),
}

return M
