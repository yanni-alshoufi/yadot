---@type table<table<string, string, string, table>> M
local M = {
    -- MOVING LINES AND WORDS
    -- Moving lines up and down with ARROW UP and ARROW DOWN
    {
        'n',
        '<Down>',
        '<CMD>move +1<CR>',
        {
            desc = 'Move Line Down',
            silent = true,
        },
    },
    {
        'n',
        '<Up>',
        '<CMD>move -2<CR>',
        {
            desc = 'Move Line Up',
            silent = true,
        },
    },
    {
        'i',
        '<Down>',
        '<Escape><CMD>move +2<CR>i',
        {
            desc = 'Move Line Down',
            silent = true,
        },
    },
    {
        'i',
        '<Up>',
        '<Escape><CMD>move -2<CR>i',
        {
            desc = 'Move Line Up',
            silent = true,
        },
    },
    -- Moving words left and right with ARROW LEFT and ARROW RIGHT
    {
        'n',
        '<Right>',
        '"mdiw"_xea<Space><Escape>"mp',
        {
            desc = 'Move Word Right',
            silent = true,
        },
    },
    {
        'n',
        '<Left>',
        '"mdiw"_xb"mPa<Space><Escape>h',
        {
            desc = 'Move Word Left',
            silent = true,
        },
    },
    {
        'i',
        '<Right>',
        '<Escape>"mdiw"_xea<Space><Escape>"mpi',
        {
            desc = 'Move Word Right',
            silent = true,
        },
    },
    {
        'i',
        '<Left>',
        '<Escape>"mdiw"_xb"mPa<Space><Escape>hi',
        {
            desc = 'Move Word Left',
            silent = true,
        },
    },

    -- DO TO ALL
    -- Copy all
    {
        'n',
        'y<C-a>',
        '<CMD>%y<CR>',
        {
            desc = 'Copy Entire File',
            silent = true,
        },
    },
    -- Select all
    {
        'n',
        'v<C-a>',
        'gg0vG$',
        {
            desc = 'Select Entire File',
            silent = true,
        },
    },
    -- Delete all
    {
        'n',
        'd<C-a>',
        '<CMD>%d<CR>',
        {
            desc = 'Delete Entire File',
            silent = true,
        },
    },

    -- Tree
    {
        'n',
        '<C-/>',
        '<CMD>NvimTreeToggle<CR>',
        {
            desc = 'Toggle Tree',
            silent = true,
        },
    },
    {
        'i',
        '<C-/>',
        '<Escape><CMD>NvimTreeToggle<CR>i',
        {
            desc = 'Toggle Tree',
            silent = true,
        },
    },

    -- Notes
    {
        'n',
        '<leader>nn',
        '<CMD>lua YaOpenNote()<CR>',
        {
            desc = 'Open YA [N]ote',
            silent = true,
        },
    },

    {
        'n',
        '<leader>na',
        '<CMD>lua YaAddNote()<CR>',
        {
            desc = '[A]dd YA [N]ote',
            silent = true,
        },
    },

    {
        'n',
        '<leader>nd',
        '<CMD>lua YaDeleteNote()<CR>',
        {
            desc = '[D]elete YA [N]ote',
            silent = true,
        },
    },

    -- Theme switching
    {
        'n',
        '<leader>yt',
        '<CMD>lua YaThemeSwitcher()<CR>',
        {
            desc = 'Open YA [T]heme Switcher',
            silent = true,
        },
    },

    -- Quick config access
    {
        'n',
        '<leader>yc',
        '<CMD>lua YaConfigMainFiles()<CR>',
        {
            desc = 'Access YA Quick [C]onfiguration',
            silent = true,
        },
    },

    {
        'n',
        '<leader>yv',
        '<CMD>lua YaConfigAllFiles()<CR>',
        {
            desc = 'Access all N[v]im Config Files',
            silent = true,
        },
    },
    {
        'n',
        '<leader>yV',
        '<CMD>lua YaConfigAllGrep()<CR>',
        {
            desc = 'Acess all N[v]im Config Files via Grep',
            silent = true,
        },
    },

    -- Commit `.config`
    {
        'n',
        '<leader>ypg',
        '<CMD>!(cd /Users/yanni/yadot/ && git add . && git commit -am "Change to Dot Files" && git push)<CR>',
        {
            desc = '[P]ersist YA Dot [G]it',
        },
    },

    -- Switch to project
    {
        'n',
        '<leader>ypp',
        '<CMD>lua YaProjSearch()<CR>',
        { silent = true },
    },

    -- Make new project
    {
        'n',
        '<leader>ypa',
        '<CMD>lua YaCreateProject()<CR>',
        { silent = true },
    },

    -- Move project to project trash
    {
        'n',
        '<leader>ypd',
        '<CMD>lua YaDeleteProject()<CR>',
        { silent = true },
    },

    -- Wrotes
    {
        'n',
        '<leader>n\\',
        '<CMD>lua YaOpenWrote()<CR>',
        { silent = true },
    },

    {
        'n',
        '<leader>n\\',
        '<CMD>lua YaAddWrote()<CR>',
        { silent = true },
    },

    {
        'n',
        '<leader>n\\',
        '<CMD>lua YaDeleteWrote()<CR>',
        { silent = true },
    },

    -- Past last deleted
    {
        { 'n', 'v' },
        '<C-p>',
        '"0p',
        {
            desc = 'Paste Last Deleted',
            silent = true,
        },
    },
}

return M
