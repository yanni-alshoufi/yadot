local telescope = require 'telescope.builtin'

local wrotes_path = '/Users/yanni/writing'
local wrote_extention = 'md'

require 'custom.input-helpers.yes-no'

function YaOpenWrote()
    telescope.find_files {
        cwd = wrotes_path,
    }
end

function YaAddWrote()
    local name = vim.fn.input 'Name (without extention): '
    local path = wrotes_path .. vim.trim(name) .. '.' .. wrote_extention

    os.execute("echo '# " .. vim.trim(name) .. "' > '" .. path .. "'")

    YaYesNoInp('open wrote', function()
        vim.cmd('e ' .. path)
    end)
end

function YaDeleteWrote()
    YaDoWithFileInDir(wrotes_path, function(path)
        YaYesNoInp('delete wrote', function()
            os.execute("rm '" .. path .. "'")
        end)
    end)
end
