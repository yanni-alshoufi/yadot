function YaYesNoInp(what_to_do, fn_when_yes, fn_when_no)
    local open_note_prompt = vim.fn.input('Press "y" or "yes" (case not sensitive) to ' .. what_to_do .. ': ')
    open_note_prompt = vim.trim(open_note_prompt)
    open_note_prompt = string.lower(open_note_prompt)

    if open_note_prompt == 'y' or open_note_prompt == 'yes' then
        fn_when_yes()
    else
        if fn_when_no then
            fn_when_no()
        end
    end
end
