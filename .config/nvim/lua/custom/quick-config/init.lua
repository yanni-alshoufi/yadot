function YaConfigAllFiles()
    require('telescope.builtin').find_files {
        cwd = '/Users/yanni/.config/nvim',
    }
end

function YaConfigAllGrep()
    require('telescope.builtin').grep_string {
        cwd = '/Users/yanni/.config/nvim',
    }
end

function YaConfigMainFiles(opts)
    opts = opts or {}

    require 'custom.general-telescope-extentions.telescope-functions'

    TelescopePickers.new(opts, {
        finder = TelescopeFinders.new_table {
            results = require 'custom.quick-config.quick-access-files',
        },
        sorter = TelescopeConf.generic_sorter(opts),
        attach_mappings = function(buffer_number, _)
            TelescopeActions.select_default:replace(function()
                TelescopeActions.close(buffer_number)
                local name = TelescopeActionState.get_selected_entry()[1]
                vim.cmd('e ' .. name)
            end)
            return true
        end,
    }):find()
end
