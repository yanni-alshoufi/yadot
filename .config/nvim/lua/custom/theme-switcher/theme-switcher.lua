function YaThemeSwitcher(opts)
    opts = opts or {}

    require 'custom.general-telescope-extentions.telescope-functions'

    local themes = vim.fn.getcompletion('', 'color')
    TelescopePickers.new(opts, {
        finder = TelescopeFinders.new_table {
            results = themes,
        },
        sorter = TelescopeConf.generic_sorter(opts),
        attach_mappings = function(buffer_number, _)
            TelescopeActions.select_default:replace(function()
                TelescopeActions.close(buffer_number)
                local selection = TelescopeActionState.get_selected_entry()

                local file = io.open('/Users/yanni/.config/nvim/lua/custom/theme-switcher/theme.txt', 'w')
                file:write(vim.trim(selection[1]))
                file:close()

                vim.cmd.colorscheme(selection[1])
            end)
            return true
        end,
    }):find()
end
