function YaDoWithFileInDir(dir, fn, include_this_and_parent, opts)
    opts = opts or {}
    include_this_and_parent = include_this_and_parent or false

    if dir:sub(-1) ~= '/' then
        dir = dir .. '/'
    end

    if not opts.propmt then
        opts.propmt = dir
    end

    require 'custom.general-telescope-extentions.telescope-functions'

    local ls_all_modifier = 'A'

    if include_this_and_parent then
        ls_all_modifier = 'a'
    end

    local handle = io.popen('(cd ' .. dir .. '; /bin/ls -' .. ls_all_modifier .. ')')
    local paths = {}
    local i = 0
    for line in handle:lines() do
        i = i + 1
        line = vim.trim(line)

        paths[i] = line
    end

    TelescopePickers.new(opts, {
        finder = TelescopeFinders.new_table {
            results = paths,
        },
        sorter = TelescopeConf.generic_sorter(opts),
        attach_mappings = function(buffer_number, _)
            TelescopeActions.select_default:replace(function()
                TelescopeActions.close(buffer_number)

                local name = TelescopeActionState.get_selected_entry()[1]

                fn(dir .. name)
            end)
            return true
        end,
    }):find()
end
