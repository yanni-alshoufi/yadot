# variables
set -x vim '/Users/yanni/.config/nvim'
set -x school '/Users/yanni/Library/Mobile Documents/com~apple~CloudDocs/A-Important/School/2324/Subjects/'
set -x prog '/Users/yanni/Quick_Prog'
set -x proj '/Users/yanni/Library/Mobile Documents/com~apple~CloudDocs/A-Important/Programming_Projects'

# vim bindings
fish_vi_key_bindings

# aliases
alias rm='rm -i'
alias mv='mv -i'
alias scim=sc-im
alias vim=nvim
alias cd..='cd ..'
alias ls='exa -lah'
alias ll='exa -ah'
alias print='printf "%s\n"'
alias grep='rg'

# use correct versions
alias rustc='/Users/yanni/.cargo/bin/rustc'
alias cargo='/Users/yanni/.cargo/bin/cargo'

# setting nvim as editor
set -x EDITOR nvim

# setting tmux colors (not sure if this is necessary)
set -x TERM tmux-256color

# setup for gtk
set -x PKG_CONFIG_PATH "/opt/homebrew/Cellar/gtk4/4.12.5/lib/pkgconfig/" "/opt/homebrew/Cellar/graphene/1.10.8/lib/pkgconfig"

# path
fish_add_path '/Users/yanni/.cargo/bin'
fish_add_path '/usr/local/bin'
fish_add_path '/System/Cryptexes/App/usr/bin'
fish_add_path '/usr/bin'
fish_add_path '/bin'
fish_add_path '/usr/sbin'
fish_add_path '/sbin'
fish_add_path '/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/local/bin'
fish_add_path '/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/bin'
fish_add_path '/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/appleinternal/bin'
fish_add_path '/opt/homebrew/bin'
fish_add_path '$HOME/.rvm/bin"'
fish_add_path '/Users/yanni/.rbenv/versions/2.7.0/bin'
fish_add_path '/Users/yanni/Library/Mobile Documents/com~apple~CloudDocs/A-Important/School/2324/Subjects/NSCS/Shell/run-c-programm'
fish_add_path '/usr/local/bin'
fish_add_path '/System/Cryptexes/App/usr/bin'
fish_add_path '/usr/bin'
fish_add_path '/bin'
fish_add_path '/usr/sbin'
fish_add_path '/sbin'
fish_add_path '/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/local/bin'
fish_add_path '/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/bin'
fish_add_path '/var/run/com.apple.security.cryptexd/codex.system/bootstrap/usr/appleinternal/bin'
fish_add_path '/opt/homebrew/bin'
fish_add_path '/Users/yanni/.cargo/bin'
fish_add_path '/opt/local/sbin'
fish_add_path '/opt/local/bin'
fish_add_path '/usr/local/bin'
fish_add_path '/usr/local/texlive/2023/bin/universal-darwin'
fish_add_path '/Users/yanni/Utils/SDKs/flutter/bin'
fish_add_path '/Users/yanni/Utils/SDKs/Android/cmdline-tools/bin'
fish_add_path '/Users/yanni/.config/emacs/bin'
fish_add_path '/Users/yanni/.dotnet/tools'
fish_add_path '/Users/yanni/Notes/scripts'
fish_add_path '/Users/yanni/JetBrainsScripts'
