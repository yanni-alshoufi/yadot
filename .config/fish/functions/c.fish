# copy command for cd-ing into current path
function c
  set tmp1 'cd "'
  set tmp2 (pwd)
  set tmp3 '"'
  echo "$tmp1$tmp2$tmp3" | pbcopy
end
