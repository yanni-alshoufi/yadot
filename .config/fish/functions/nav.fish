function nav
	set lastcmd ""

	while true
		clear

		test -n "$lastcmd" && echo "Last command: \`$lastcmd\`\n"

		pwd
		echo

		if type exa &> /dev/null
			exa -lah
		else 
			ls -lah
		end

		echo

		set tmp ""

		set tmp (read -S)

		set lastcmd "$tmp"

		[ $tmp = 'q' ] && break

		eval "$tmp"
	end
end

