function fish_greeting
	clear

	if [ -z "$TMUX" ] 
		set session_id (tmux list-sessions | tail -n 1 | awk 'BEGIN { FS=":" } ; { print $1 }')

		if [ -z "$session_id" ]
			tmux
		else
			tmux attach -t $session_id
		end
	end
end
